<?php
namespace kiozk\datetime\assets;

use Yii;

use yii\bootstrap4\BootstrapAsset;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\web\AssetBundle;

/**
 * Class DateRangePickerAsset
 * @package main\assets
 */
class DateRangePickerAsset extends AssetBundle
{
    public $sourcePath = '@vendor/kiozk/yii2-datetime/plugins/date-range-picker';

    public function init()
    {
        $this->js[] = YII_DEBUG ? 'date-range-picker.js' : 'date-range-picker.min.js' ;
        $this->css[] = YII_DEBUG ? 'date-range-picker.css' : 'date-range-picker.min.css' ;

    }

    public $depends = [
        JqueryAsset::class,
        BootstrapAsset::class,
        MomentAsset::class
    ];
}