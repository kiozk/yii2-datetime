<?php
namespace kiozk\datetime\assets;

use Yii;

use yii\web\View;
use yii\web\AssetBundle;

/**
 * Class DateTimePickerAsset
 * @package main\assets
 */
class MomentAsset extends AssetBundle
{
    public $sourcePath = '@vendor/moment/moment/min';

    public function init()
    {
        $this->js[] = YII_DEBUG ? 'moment-with-locales.js' : 'moment-with-locales.min.js' ;
        Yii::$app->view->registerJs('moment.locale("' . Yii::$app->language. '")', View::POS_READY,'moment-init-locale');
    }



}