<?php
namespace kiozk\datetime\traits\form;

use DateInterval;
use DatePeriod;
use DateTime;
use kiozk\datetime\helpers\DateTimeHelper;
use yii\base\InvalidArgumentException;

/**
 * Class DatePeriodModelTrait
 * @package main\traits\form
 *
 * @property int $daysSelectLimit
 * @property string $maxDate
 * @property string $period
 * @property string $interval
 * @property bool $isIntervalByDays
 * @property bool $isIntervalByHours
 */
trait DatePeriodModelTrait{

    use TimezoneModelTrait;

    /**
     * @var DatePeriod
     */
    protected $_period;

    /**
     * @var DateInterval
     */
    protected $_interval;

    /**
     * @var string
     */
    public $dateFormat = 'd.m.Y';

    /**
     * @var string
     */
    public $dateSeparator = ' - ';

    private $_maxDate;

    /**
     * @param bool $asObject
     * @return \DatePeriod|string
     * @throws \Exception
     */
    public function getPeriod($asObject = false){
        if($this->_period === null) {
            $this->_period = $this->createDefaultPeriod();
        }

        if($asObject) {
            return $this->_period;
        } else {
            return $this->_period->getStartDate()->format($this->dateFormat) .
                $this->dateSeparator .
                $this->_period->getEndDate()->format($this->dateFormat);
        }

    }

    /**
     * @param $value
     * @throws \Exception
     */
    public function setPeriod($value){
        if(is_string($value)){
            if(is_string($this->dateSeparator)) {
                $paths = explode($this->dateSeparator, $value, 2);
            } elseif (is_callable($this->dateSeparator)){
                $paths = call_user_func($this->dateSeparator);
            } else {
                $paths = null;
            }

            if(is_array($paths) && count($paths) === 2) {

                $timeZone = $this->getTimezone(true);
                if(
                    (null !== $valFrom = DateTimeHelper::createDateTimeFromFormat($this->dateFormat, $paths[0], $timeZone))
                    &&
                    (null !== $valTo  = DateTimeHelper::createDateTimeFromFormat($this->dateFormat, $paths[1], $timeZone))){
                    if($valFrom->getTimestamp() <= $valTo->getTimestamp()) {

                        $period  = DateTimeHelper::createDatePeriod($valFrom, $valTo, $this->getInterval(true),$timeZone);

                        //Не более 90 дней
                        if($period->getStartDate()->diff($period->getEndDate())->format('%a') <= 90) {
                            $this->_period = $period;
                        }
                    }
                }
            }
        } elseif ($value instanceof DatePeriod){
            $this->_period = $value;
        }
    }

    /**
     * @return DatePeriod
     * @throws \Exception
     */
    protected function createDefaultPeriod(){
        $timezone       = $this->getTimezone(true);
        $interval       = $this->getInterval(true);

        $from           = (new DateTime('-29 days', $timezone))->setTime(0, 0, 0);
        $to             = (new DateTime('now', $timezone))->setTime(23, 59, 59);


        return new DatePeriod($from, $interval, $to);
    }


    /**
     * @param bool $asObject
     * @return null
     * @throws \Exception
     */
    public function getInterval($asObject = false){
        if($this->_interval === null) {
            $this->setInterval('day');
        }

        if($asObject){
            return $this->_interval;
        }

        $stringValue =  DateTimeHelper::dateIntervalToString($this->_interval);

        if($stringValue === 'PT1H'){
            return 'hour';
        }elseif($stringValue === 'P1D'){
            return 'day';
        }else{
            return $stringValue;
        }
    }

    /**
     * @param $value
     * @throws \Exception
     */
    public function setInterval($value){
        if(is_string($value)){
            if($value === 'day'){
                $this->_interval = new DateInterval('P1D');
            } elseif ($value === 'hour'){
                $this->_interval = new DateInterval('PT1H');
            } else {
                $this->_interval = new DateInterval($value);
            }
        } elseif ($value instanceof DateInterval){
            $this->_interval = $value;
        } else {
            throw new InvalidArgumentException('Value mus be string or DateInterval object.');
        }
    }
    /**
     *
     * @param bool $asObject
     *
     * @return string|DateTime
     */
    public function getMaxDate($asObject = false){
        if($this->_maxDate === null){
            $this->_maxDate = new DateTime('now', $this->getTimezone(true));
        }

        if($asObject) {
            return $this->_maxDate;
        }

        return $this->_maxDate->format($this->dateFormat);
    }

    /**
     * @return int
     */
    public function getDaysSelectLimit(){
        return 90;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function getIsIntervalByDays(){
        return $this->getInterval() === 'day';
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function getIsIntervalByHours(){
        return $this->getInterval() === 'hour';
    }
}