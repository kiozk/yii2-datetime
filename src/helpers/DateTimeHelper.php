<?php

namespace kiozk\datetime\helpers;

use DateInterval;
use DatePeriod;
use DateTime;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use Throwable;
use yii\base\InvalidArgumentException;

class DateTimeHelper
{
    /**
     * @param int|DateTime $from
     * @param int|DateTime $to
     * @param null|DateInterval|string $interval
     * @param null|DateTimeZone|string $timezone
     * @param bool $resetTime
     * @return DatePeriod
     * @throws Exception
     */
    public static function createDatePeriod($from, $to, $interval = null, $timezone = null, $resetTime = true)
    {
        if (is_null($timezone)) {
            $timezone = new DateTimeZone('UTC');
        } elseif (is_string($timezone)) {
            try {
                $timezone = new DateTimeZone($timezone);
            } /** @noinspection PhpUndefinedClassInspection */ catch (Throwable $throwable) {
                throw new InvalidArgumentException('Invalidate timezone param value.', $throwable);
            }
        } elseif (!$timezone instanceof DateTimeZone) {
            throw new InvalidArgumentException('Invalidate timezone param value.');
        }


        if (is_null($interval)) {
            $interval = new DateInterval('P1D');
        } elseif (is_string($interval)) {
            try {
                $interval = new DateInterval($interval);
            } catch (Throwable $throwable) {
                throw new InvalidArgumentException('Invalidate $interval param value.', $throwable);
            }
        } elseif (!$interval instanceof DateInterval) {
            throw new InvalidArgumentException('Invalidate $interval argument type, allowed "null", string and instance of class "DateInterval".');
        }

        if (is_int($from)) {
            $from = (new DateTime('now', $timezone))->setTimestamp($from);

        } elseif (!$from instanceof DateTime) {
            throw new InvalidArgumentException('Invalidate $from argument type, allowed "integer" and instance of class "DateTime".');
        }

        if (is_int($to)) {
            $to = (new DateTime('now', $timezone))->setTimestamp($to);
        } elseif (!$to instanceof DateTime) {
            throw new InvalidArgumentException('Invalidate $to argument type, allowed "integer" and instance of class "DateTime".');
        }

        if ($resetTime) {
            $to->setTime(23, 59, 59);
            $from->setTime(0, 0, 0);
        }


        return new DatePeriod($from, $interval, $to);
    }

    /**
     * @param string|DateTimeZone $timezone
     * @param bool $withToday
     * @return DatePeriod
     * @throws Exception
     */
    public static function createDatePeriodForLast30Days($timezone = null, $withToday = true)
    {
        return $withToday ?
            DateTimeHelper::createDatePeriod(time() - 3600 * 24 * 29, time(), null, $timezone)
            :
            DateTimeHelper::createDatePeriod(time() - 3600 * 24 * 30, time() - 3600 * 24, null, $timezone);
    }

    /**
     * @param string|DateTimeZone $timezone
     * @param bool $withToday
     * @return DatePeriod
     * @throws Exception
     */
    public static function createDatePeriodForPrev30Days($timezone = null, $withToday = true)
    {
        return $withToday ?
            DateTimeHelper::createDatePeriod(time() - 3600 * 24 * 29 - 3600 * 24 * 30, time() - 3600 * 24 * 30, null, $timezone)
            :
            DateTimeHelper::createDatePeriod(time() - 3600 * 24 * 30 - 3600 * 24 * 30, time() - 3600 * 24 - 3600 * 24 * 30, null, $timezone);
    }


    /**
     * @param $format
     * @param $value
     * @param $timeZone
     * @return DateTime|null
     */
    public static function createDateTimeFromFormat($format, $value, $timeZone = null)
    {
        if ($timeZone === null) {
            $timeZone = new DateTimeZone('UTC');
        }
        if (false === $dateTime = date_create_from_format($format, $value, $timeZone)) {
            return null;
        }

        return $dateTime;
    }

    public static function nowStart()
    {
        $tz = new DateTimeZone('UTC');
        return (new DateTime('now', $tz))->setTime(0, 0, 0);
    }

    public static function nowEnd()
    {
        $tz = new DateTimeZone('UTC');
        return (new DateTime('now', $tz))->setTime(0, 0, 0);
    }

    /**
     * @var array
     * @return array
     */
    public static function timeZoneDropDown()
    {
        if (false === $timeZoneIdentifiers = @DateTimeZone::listIdentifiers(
                DateTimeZone::AFRICA |
                DateTimeZone::AMERICA |
                DateTimeZone::ANTARCTICA |
                DateTimeZone::ARCTIC |
                DateTimeZone::ASIA |
                DateTimeZone::ATLANTIC |
                DateTimeZone::AUSTRALIA |
                DateTimeZone::EUROPE |
                DateTimeZone::INDIAN |
                DateTimeZone::PACIFIC
            )) {
            return [];
        };

        $dropDownArray = [];
        $baseDate = new DateTime('now', new DateTimeZone('UTC'));
        foreach ($timeZoneIdentifiers as $timeZoneIdentifier) {
            $explodes = explode('/', $timeZoneIdentifier);
            if (count($explodes) === 2) {

                $tz = new DateTimeZone($timeZoneIdentifier);
                $offset = $tz->getOffset($baseDate);
                $hours = abs(floor($offset / 3600));
                $minutes = abs(floor($offset / 60 % 60));
                $dropDownArray[$explodes[0]][$timeZoneIdentifier] = strtr($explodes[1], [
                        '_' => ' '
                    ]) . ' (GMT' . sprintf('%s%d:%02d', $offset < 0 ? '-' : '+', $hours, $minutes) . ')';
            }
        }

        return $dropDownArray;
    }


    /**
     * @param DateInterval $dateInterval
     * @return int
     * @throws Exception
     */
    public static function getTotalSecondsFromInterval(DateInterval $dateInterval)
    {
        $reference = new DateTimeImmutable;
        $endTime = $reference->add($dateInterval);
        return $endTime->getTimestamp() - $reference->getTimestamp();
    }

    /**
     * @param $value
     * @param DateTimeZone|null $timeZone
     * @return int|null
     */
    public static function asInt($value, $timeZone = null)
    {
        if (is_string($timeZone)) {
            try {
                $timeZone = new DateTimeZone($timeZone);
            } catch (Exception $e) {
                $timeZone = new DateTimeZone('UTC');
            }
        }

        if (is_string($value)) {
            if (ctype_digit($value)) {
                return intval($value);
            } else {
                if (false !== $dateTime = DateTime::createFromFormat('Y.m.d H:i', $value, $timeZone)) {
                    return $dateTime->getTimestamp();
                }
            }
        } elseif (is_int($value)) {
            return $value;
        } elseif ($value instanceof DateTime) {
            return $value->getTimestamp();
        }
        return null;
    }

    /**
     * @param $value
     * @param DateTimeZone|null $timeZone
     * @return bool|DateTime|null
     */
    public static function asDateTime($value, $timeZone = null)
    {

        if (is_string($timeZone)) {
            try {
                $timeZone = new DateTimeZone($timeZone);
            } catch (Exception $e) {
                $timeZone = new DateTimeZone('UTC');
            }
        }

        if (is_null($value)) {
            return null;
        } elseif (is_string($value)) {
            if (ctype_digit($value)) {
//                return new DateTime('@'. $value, $timeZone);
                return (new DateTime('now', $timeZone))->setTimestamp($value);
            } else {
                if (false !== $dateTime = DateTime::createFromFormat('Y.m.d H:i', $value)) {
                    return $dateTime;
                }

                if (false !== $dateTime = DateTime::createFromFormat('Y.m.d H:i:s', $value)) {
                    return $dateTime;
                }
            }
        } elseif ($value instanceof DateTime) {
            return $value;
        } elseif (is_int($value) && $value >= 0) {
//            return new DateTime('@'. $value, $timeZone);
            return (new DateTime('now', $timeZone))->setTimestamp($value);
        }

        return null;
    }

    /**
     * Возвращает экземпляр зоны UTC
     *
     * @return DateTimeZone
     */
    public static function createUtcTimeZone()
    {
        return new DateTimeZone('UTC');
    }

    /**
     * @param DatePeriod $period
     *
     * @return DatePeriod
     */
    public static function toPreviousPeriod(DatePeriod $period): DatePeriod
    {

        /**
         * @var DateTime $startDate
         * @var DateTime $endDate
         */
        $startDate = clone $period->getStartDate();
        $endDate = clone $period->getEndDate();

        $diffInterval = $endDate->diff($startDate);

        $startDate = $startDate->add($diffInterval)->sub($period->getDateInterval());
        $endDate = $endDate->add($diffInterval)->sub($period->getDateInterval());

        return new DatePeriod($startDate, $period->getDateInterval(), $endDate);
    }

    /**
     * Создание периуда
     *
     * @param string|int|float|DateTime $from
     * @param string|int|float|DateTime $to
     * @param string|DateInterval $interval
     * @param null|DateTimeZone $timezone
     *
     * @return DatePeriod
     * @throws Exception
     */
    public static function period($from, $to, $interval = 'P1D', $timezone = null): DatePeriod
    {
        if (is_string($timezone)) {
            $timezone = new DateTimeZone($timezone);
        } elseif (!is_null($timezone) && !$timezone instanceof DateTimeZone) {
            throw new InvalidArgumentException('Parameter from "$timezone" mast be null, string or DateTimeZone.');
        }

        if (is_int($from)) {
            $startDate = new DateTime('@' . $from, $timezone);
        } elseif (is_float($from)) {
            $startDate = new DateTime('@' . intval($from), $timezone);
        } elseif (is_string($from)) {
            $startDate = new DateTime($from, $timezone);
        } elseif (!$from instanceof DateTime) {
            throw new InvalidArgumentException('Parameter from "$from" mast be int, float, string or DateTime.');
        }

        if (is_int($to)) {
            $endDate = new DateTime('@' . $to, $timezone);
        } elseif (is_float($to)) {
            $endDate = new DateTime('@' . intval($to), $timezone);
        } elseif (is_string($to)) {
            $endDate = new DateTime($to, $timezone);
        } elseif (!$to instanceof DateTime) {
            throw new InvalidArgumentException('Parameter from "$to" mast be int, float, string or DateTime.');
        }

        if (is_string($interval)) {
            $interval = new DateInterval($interval);
        } elseif (!$interval instanceof DateInterval) {
            throw new InvalidArgumentException('Parameter from "$interval" mast be string or DateInterval.');
        }


        /**
         * @var DateTime $startDate
         * @var DateTime $endDate
         */
        return new DatePeriod($startDate, $interval, $endDate);
    }

    /**
     * @param null $timezone
     * @return DatePeriod
     * @throws Exception
     */
    public static function periodLast30Days($timezone = null)
    {

        return static::period('-29 days 00:00:00', 'today 23:59:59', 'P1D', $timezone);
    }

    /**
     * @param DateInterval $interval
     * @return string
     */
    public static function dateIntervalToString($interval)
    {
        $result = 'P';
        if ($interval->y) {
            $result .= $interval->y . 'Y';
        }

        if ($interval->m) {
            $result .= $interval->m . 'M';
        }

        if ($interval->d) {
            $result .= $interval->d . 'D';
        }

        if ($interval->h || $interval->i || $interval->s) {
            $result .= 'T';

            if ($interval->h) {
                $result .= $interval->h . 'H';
            }

            if ($interval->i) {
                $result .= $interval->i . 'M';
            }

            if ($interval->s) {
                $result .= $interval->s . 'S';
            }
        }

        return $result;
    }
}