<?php
namespace kiozk\datetime\widgets;


use common\helpers\Html;
use kiozk\datetime\assets\DateRangePickerAsset;
use Yii;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\widgets\InputWidget;

class DateRangePicker extends InputWidget
{
    public $static = false;

    /**
     * @var array widget plugin options
     */
   // public $pluginOptions = [];

    public $ranges = true;

    private $_format = 'DD.MM.YYYY';

    public $separator = '';

    /**
     * @var string|\DatePeriod
     */
    public $value;

    public function run()
    {
        $this->registerAssets();
        $id = isset($this->options['id']) ? $this->options['id'] : $this->getId();

        $view = $this->view;

        $pluginOptions = [
            'locale' => [
                'applyLabel' => Yii::t('app', 'Apply'),
                'cancelLabel' => Yii::t('app', 'Cancel'),
                'customRangeLabel' => Yii::t('app', 'Custom range'),
            ]
        ];
        if($this->ranges){
            $pluginOptions['ranges'] = [
                Yii::t('app', 'Today') => new JsExpression('[moment(), moment()]'),
                Yii::t('app', 'Yesterday') => new JsExpression('[moment().subtract(1, \'days\'), moment().subtract(1, \'days\')]'),
                Yii::t('app', 'Last 7 Days') => new JsExpression('[moment().subtract(6, \'days\'), moment()]'),
                Yii::t('app', 'Last 30 Days') => new JsExpression('[moment().subtract(29, \'days\'), moment()]'),
                Yii::t('app', 'Last 90 Days') => new JsExpression('[moment().subtract(89, \'days\'), moment()]'),
                Yii::t('app', 'This Month') => new JsExpression('[moment().startOf(\'month\'), moment().endOf(\'month\')]'),
                Yii::t('app', 'Last Month') => new JsExpression('[moment().subtract(1, \'month\').startOf(\'month\'), moment().subtract(1, \'month\').endOf(\'month\')]'),
            ];
        }

        $format     = $this->_format;
        $separator  = $this->separator;
        $phpFormat = static::convertDateFormat($format, true);
        $formatJs   = Json::encode($format);
        $separatorJs = Json::encode($this->separator);
        if($this->static){
            $callback = /** @lang JavaScript */ "
function(start, end) {
    var val = start.format({$formatJs}) + $separatorJs + end.format({$formatJs});
    $('#{$id}').val(val).trigger('changed');
    $('#{$id}-value').text(val);
}
";
        }

        $options = $this->options;
        $options['id'] = $id . '-wrapper';
        if ($this->hasModel()) {
            if(is_object($this->value)){
                $value = $this->value;
            } else {
                $value = isset($options['value']) ? $options['value'] : Html::getAttributeValue($this->model, $this->attribute);

            }
        } else {
            $value = $this->value;
        }


        if(is_object($value)) {

            if($value instanceof \DatePeriod) {
                $pluginOptions['startDate'] = new JsExpression('new Date('.$value->getStartDate()->getTimestamp().'000)');
                $pluginOptions['endDate'] = new JsExpression('new Date('.$value->getEndDate()->getTimestamp().'000)');

                $value = $value->getStartDate()->format($phpFormat) . $separator . $value->getEndDate()->format($phpFormat);
            }

        } else {
           // $dates = explode($this->_separator, $this->value);
            //if (count($dates) > 1) {
             //   $this->pluginOptions['startDate'] = $dates[0];
           //     $this->pluginOptions['endDate'] = $dates[1];
         //   }
        }
        $pluginOptionsJson = Json::encode($pluginOptions);


        Html::addCssClass($options, 'btn btn-secondary dropdown-toggle');
        if($this->static){

            $input = $this->renderInputHtml('hidden');
            $view->registerJs("$('#{$id}-wrapper').dateRangePicker({$pluginOptionsJson}".(empty($callback) ? '' : ', ' . $callback) .")");


            return Html::tag('div', $input . Html::tag('span', $value, ['id' => $id . '-value'] ), $options);
        } else {
            $view->registerJs("$('#{$id}').dateRangePicker({$pluginOptionsJson}".(empty($callback) ? '' : ', ' . $callback) .")");

            return $this->renderInputHtml('text');
        }
    }

    protected function registerAssets(){
        $view = $this->view;
        DateRangePickerAsset::register($view);
    }

    public function setFormat($format){
        $this->_format = static::convertDateFormat($format);
    }

    /**
     * Automatically convert the date format from PHP DateTime to Moment.js DateTime format as required by
     * the `bootstrap-daterangepicker` plugin.
     *
     * @see http://php.net/manual/en/function.date.php
     * @see http://momentjs.com/docs/#/parsing/string-format/
     *
     * @param string $format the PHP date format string
     * @param string $toPhp
     *
     * @return string
     */
    protected static function convertDateFormat($format, $toPhp = false)
    {
        $a = [
            // meridian lowercase remains same
            // 'a' => 'a',
            // meridian uppercase remains same
            // 'A' => 'A',
            // second (with leading zeros)
            's' => 'ss',
            // minute (with leading zeros)
            'i' => 'mm',
            // hour in 12-hour format (no leading zeros)
            'g' => 'h',
            // hour in 12-hour format (with leading zeros)
            'h' => 'hh',
            // hour in 24-hour format (no leading zeros)
            'G' => 'H',
            // hour in 24-hour format (with leading zeros)
            'H' => 'HH',
            //  day of the week locale
            'w' => 'e',
            //  day of the week ISO
            'W' => 'E',
            // day of month (no leading zero)
            'j' => 'D',
            // day of month (two digit)
            'd' => 'DD',
            // day name short
            'D' => 'DDD',
            // day name long
            'l' => 'DDDD',
            // month of year (no leading zero)
            'n' => 'M',
            // month of year (two digit)
            'm' => 'MM',
            // month name short
            'M' => 'MMM',
            // month name long
            'F' => 'MMMM',
            // year (two digit)
            'y' => 'YY',
            // year (four digit)
            'Y' => 'YYYY',
            // unix timestamp
            'U' => 'X',
        ];
        if($toPhp){
            $a = array_flip($a);
        }
        return strtr($format, $a);
    }

}