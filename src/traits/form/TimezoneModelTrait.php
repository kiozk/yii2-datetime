<?php
namespace kiozk\datetime\traits\form;

use DateTimeZone;
use /** @noinspection PhpUndefinedClassInspection */
    Throwable;
use yii\base\InvalidArgumentException;

trait TimezoneModelTrait{
    /**
     * @var DateTimeZone
     */
    private $_timezone;

    public function getTimezone($asObject = false){
        if($this->_timezone === null) {
            $this->_timezone = new DateTimeZone(date_default_timezone_get());
        }

        if($asObject) {
            return $this->_timezone;
        } else {
            return $this->_timezone->getName();
        }
    }

    public function setTimezone($value){
        if(is_string($value)){
            try{
                $this->_timezone = new DateTimeZone($value);
            } /** @noinspection PhpUndefinedClassInspection */ catch (Throwable $throwable){
                throw new InvalidArgumentException('Timezone value invalidate string value.');
            }
        } elseif($value instanceof DateTimeZone ){
            $this->_timezone = $value;
        } else {
            throw new InvalidArgumentException('Timezone value mast be string or timezone.');
        }
    }

}